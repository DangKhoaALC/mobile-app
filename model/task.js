const {Model} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
    class Task extends Model{
        static associate(models){
            Task.belongsTo(models.User, {foreignkey: 'userId'})
        }
    };
    Task.init({
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        expiration: DataTypes.DATE,
        status: DataTypes.STRING,
        notification: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: 'Task',
    });
  return Task;
};
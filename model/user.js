const {Model} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
    class User extends Model{
        static associate(models){
           User.hasMany(models.Task, {foreignkey: 'usreId'})
        }
    };
   User.init({
       email: DataTypes.STRING,
       name: DataTypes.STRING,
       password: DataTypes.STRING,
       access: DataTypes.STRING,
       avatar: DataTypes.STRING
   },
   {
       sequelize,
       modelNAME: 'User'
   });
   return User;
};